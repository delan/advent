Intcode disassembler
cargo run --bin intcode dump < program.txt

Intcode general virtual machine
cargo run --bin 2019_09 program.txt

2019 day 1 part 1 (fuel where fuel is free)
cargo run --bin 2019_01_1 < input.txt

2019 day 1 part 2 (fuel taking fuel into account)
cargo run --bin 2019_01_2 < input.txt

2019 day 2 part 1 (initial Intcode virtual machine)
cargo run --bin 2019_02_1 < program.txt

2019 day 2 part 2 (Intcode: reset and brute force)
cargo run --bin 2019_02_2 < program.txt

2019 day 3 part 1 (Crossed Wires with Manhattan distance)
cargo run --bin 2019_03_1 < input.txt

2019 day 3 part 2 (Crossed Wires with wire distance)
cargo run --bin 2019_03_2 < input.txt

2019 day 4 part 1 (password counting with loose doubles)
cargo run --bin 2019_04_1 < input.txt

2019 day 4 part 2 (password counting with strict doubles)
cargo run --bin 2019_04_2 < input.txt

2019 day 5 part 1 (Intcode: signed integers + in + out)
cargo run --bin 2019_05_1 program.txt < input.txt

2019 day 5 part 2 (Intcode: jnz + jz + setl + sete)
cargo run --bin 2019_05_2 program.txt < input.txt

2019 day 6 part 1 (Universal Orbit Map: orbit count checksum)
cargo run --bin 2019_06_1 < input.txt

2019 day 6 part 2 (Universal Orbit Map: orbital transfers)
cargo run --bin 2019_06_2 < input.txt

2019 day 7 part 1 (Intcode: amplifier settings brute force)
cargo run --bin 2019_07_1 program.txt

2019 day 7 part 2 (Intcode: amplifiers with feedback loops)
cargo run --bin 2019_07_2 program.txt

2019 day 8 part 1 (Space Image Format statistics)
cargo run --bin 2019_08_1 < input.txt

2019 day 8 part 2 (Space Image Format compositing)
cargo run --bin 2019_08_2 < input.txt > image.xpm

2019 day 9 part 1 (Intcode: BOOST program test mode)
echo 1 | cargo run --bin 2019_09 program.txt

2019 day 9 part 1 (Intcode: BOOST program boost mode)
echo 2 | cargo run --bin 2019_09 program.txt
