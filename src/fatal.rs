use std::fmt::{Debug, Display};

use jane_eyre::eyre::{eyre, WrapErr};

pub trait FatalWrapErr<T, E> {
    fn fatal(self, msg: impl Display + Send + Sync + 'static) -> T;
}

pub trait FatalOption<T> {
    fn fatal(self, msg: impl Display + Debug + Send + Sync + 'static) -> T;
}

impl<T, E: std::error::Error + Send + Sync + 'static, W: WrapErr<T, E>> FatalWrapErr<T, E> for W {
    fn fatal(self, msg: impl Display + Send + Sync + 'static) -> T {
        self.wrap_err(msg).unwrap()
    }
}

impl<T> FatalOption<T> for Option<T> {
    fn fatal(self, msg: impl Display + Debug + Send + Sync + 'static) -> T {
        self.ok_or_else(|| eyre!(msg)).unwrap()
    }
}
