use std::io::{Read, Write};

use failure::{bail, Error};

pub struct Program<I> {
    inner: I,
}

impl<I> Program<I> {
    pub fn new(inner: I) -> Self {
        Self { inner }
    }
}

impl<I: Read> Iterator for Program<I> {
    type Item = Result<isize, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut result: Option<isize> = None;
        let mut factor = 1;

        loop {
            result = match digit(&mut self.inner) {
                Ok(Some(q)) if q == b'-'.into() => {
                    factor = -1;

                    result
                }
                Ok(Some(q)) => result.map_or_else(
                    || Some(q - isize::from(b'0')),
                    |p| Some(q - isize::from(b'0') + p * 10),
                ),
                Ok(None) => return result.map(|x| Ok(x * factor)),
                Err(x) => return Some(Err(x)),
            };
        }

        fn digit(source: &mut impl Read) -> Result<Option<isize>, Error> {
            let mut scratch = [0u8];

            match source.read(&mut scratch) {
                Ok(0) => Ok(None),
                Ok(1) => match scratch[0] {
                    b',' | b'\n' | b'\r' => Ok(None),
                    x @ b'-' | x @ b'0'..=b'9' => Ok(Some(x.into())),
                    x => bail!("unexpected {:?}", x),
                },
                Ok(_) => unreachable!(),
                Err(x) => bail!("I/O error {:?}", x),
            }
        }
    }
}

pub fn disassemble(sink: &mut impl Write, memory: &[isize]) -> Result<(), Error> {
    let m = memory;
    let mut i = 0;

    while i < m.len() {
        let mnemonic = match m[i] % 100 {
            1 => "add",
            2 => "mul",
            3 => "in",
            4 => "out",
            5 => "jnz",
            6 => "jz",
            7 => "setl",
            8 => "sete",
            9 => "arel",
            99 => "hlt",
            _ => {
                writeln!(sink, "{:<6}  {:<22}  {}", i, m[i], "???")?;
                i += 1;
                continue;
            }
        };

        let p = match m[i] / 100 % 10 {
            0 => "$",
            1 => "",
            2 => "$$",
            _ => {
                writeln!(sink, "{:<6}  {:<22}  {}", i, m[i], "???")?;
                i += 1;
                continue;
            }
        };

        let q = match m[i] / 1000 % 10 {
            0 => "$",
            1 => "",
            2 => "$$",
            _ => {
                writeln!(sink, "{:<6}  {:<22}  {}", i, m[i], "???")?;
                i += 1;
                continue;
            }
        };

        let r = match m[i] / 10000 % 10 {
            0 => "$",
            1 => "",
            2 => "$$",
            _ => {
                writeln!(sink, "{:<6}  {:<22}  {}", i, m[i], "???")?;
                i += 1;
                continue;
            }
        };

        let modes = vec![p, q, r];

        let operands = match m[i] % 100 {
            1 | 2 | 7 | 8 => vec![m[i + 1], m[i + 2], m[i + 3]],
            3 | 4 | 9 => vec![m[i + 1]],
            5 | 6 => vec![m[i + 1], m[i + 2]],
            99 => vec![],
            x => bail!("bad opcode {}", x),
        };

        let delta = 1 + operands.len();

        writeln!(
            sink,
            "{:<6}  {:<22}  {:<6}  {:<24}",
            i,
            m[i..(i + delta)]
                .iter()
                .map(|x| format!("{}", x))
                .collect::<Vec<_>>()
                .join(" "),
            mnemonic,
            modes
                .iter()
                .zip(operands)
                .map(|(m, o)| format!("{}{}", m, o))
                .collect::<Vec<_>>()
                .join(", "),
        )?;

        i += delta;
    }

    Ok(())
}
