use regex::Regex;

use crate::fatal::FatalOption;

pub trait CapturesOwned {
    fn captures_owned(&self, input: &str) -> Vec<String>;
}

impl CapturesOwned for Regex {
    #[rustfmt::skip]
    fn captures_owned(&self, input: &str) -> Vec<String> {
        self.captures(input)
            .fatal("input failed to match pattern")
            .iter()
            .map(|x| x.fatal("input failed to yield capture group").as_str().to_owned())
            .skip(1) // skip capture of entire match
            .collect()
    }
}
