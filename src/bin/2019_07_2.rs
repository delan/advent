use std::cmp::max;
use std::convert::{TryFrom, TryInto};
use std::env::args;
use std::fs::File;
use std::io::{BufReader, Read, Write};
use std::iter::repeat;
use std::str;

use failure::{bail, Error};

use advent::intcode::Program;

fn main() -> Result<(), Error> {
    let p = BufReader::new(File::open(args().nth(1).unwrap())?);
    let p: Vec<_> = Program::new(p).collect::<Result<_, _>>()?;

    let mut x = (0..5).collect::<Vec<_>>();
    let mut result = thrusters(&p, &shift(&x, 5))?;

    while plain_changes(&mut x) {
        result = max(result, thrusters(&p, &shift(&x, 5))?);
    }

    println!("{}", result);

    Ok(())
}

fn shift(permutation: &[isize], delta: isize) -> Vec<isize> {
    permutation.iter().copied().map(|x| x + delta).collect()
}

fn plain_changes(permutation: &mut [isize]) -> bool {
    let n = permutation.len();
    let mut x = repeat(0).take(n).collect::<Vec<_>>();
    let mut y = repeat(0).take(n).collect::<Vec<_>>();

    for (p, &i) in permutation.iter().enumerate() {
        let stop = i - 1;
        let i = usize::try_from(i).unwrap();
        x[i] = isize::try_from(p).unwrap();
        y[i] = x[i] - signum(permutation, stop);
    }

    for i in (0..n).rev() {
        let n = n.try_into().unwrap();

        if 0 <= y[i] && y[i] < n {
            let y = y[i].try_into().unwrap();

            if permutation[y] < i.try_into().unwrap() {
                let x = x[i].try_into().unwrap();
                permutation.swap(x, y);
                return true;
            }
        }
    }

    fn signum(permutation: &[isize], maximum: isize) -> isize {
        let mut result = 0;

        for i in 0..permutation.len() {
            for j in (i + 1)..permutation.len() {
                if maximum >= permutation[i] && permutation[i] > permutation[j] {
                    result += 1;
                }
            }
        }

        1 - result % 2 * 2
    }

    false
}

fn thrusters(program: &[isize], settings: &[isize]) -> Result<isize, Error> {
    let mut result = 0;

    let mut memories = settings
        .iter()
        .map(|_| program.to_owned())
        .collect::<Vec<_>>();

    let mut counters = settings.iter().map(|_| 0).collect::<Vec<_>>();

    let mut settings = settings.iter().copied().map(Some).collect::<Vec<_>>();

    for i in (0..settings.len()).cycle() {
        if let Some(signal) = amplify(
            &mut memories[i],
            &mut counters[i],
            settings[i].take(),
            result,
        )? {
            result = signal;
        } else {
            break;
        }
    }

    Ok(result)
}

fn amplify(
    memory: &mut [isize],
    counter: &mut usize,
    setting: Option<isize>,
    signal: isize,
) -> Result<Option<isize>, Error> {
    let input = if let Some(setting) = setting {
        format!("{},{}", setting, signal)
    } else {
        format!("{}", signal)
    };

    let mut output = vec![];

    if run(memory, counter, &mut input.as_bytes(), &mut output)? {
        return Ok(Some(isize::from_str_radix(
            str::from_utf8(&output)?.trim(),
            10,
        )?));
    }

    Ok(None)
}

fn run(
    memory: &mut [isize],
    counter: &mut usize,
    input: &mut impl Read,
    mut output: &mut impl Write,
) -> Result<bool, Error> {
    let mut m = memory;
    let i = counter;
    let mut input = Program::new(input);

    loop {
        if *i >= m.len() {
            bail!("program counter {} out of bounds", *i);
        }

        let mut jump = None;
        let mut out = false;

        *i += match m[*i] % 100 {
            /* add */
            1 => {
                let (p, q) = (m[*i] / 100 % 10, m[*i] / 1000 % 10);
                let (x, y) = (load(&m, m[*i + 1], p)?, load(&m, m[*i + 2], q)?);
                let address = m[*i + 3];
                store(&mut m, address, x + y)?;

                4
            }
            /* mul */
            2 => {
                let (p, q) = (m[*i] / 100 % 10, m[*i] / 1000 % 10);
                let (x, y) = (load(&m, m[*i + 1], p)?, load(&m, m[*i + 2], q)?);
                let address = m[*i + 3];
                store(&mut m, address, x * y)?;

                4
            }
            /* in */
            3 => {
                let address = m[*i + 1];
                store(&mut m, address, read(&mut input)?)?;

                2
            }
            /* out */
            4 => {
                let p = m[*i] / 100 % 10;
                write(&mut output, load(&m, m[*i + 1], p)?)?;
                out = true;

                2
            }
            /* jnz */
            5 => {
                let (p, q) = (m[*i] / 100 % 10, m[*i] / 1000 % 10);
                let (x, y) = (load(&m, m[*i + 1], p)?, load(&m, m[*i + 2], q)?);

                if x != 0 {
                    jump = Some(y);
                }

                3
            }
            /* jz */
            6 => {
                let (p, q) = (m[*i] / 100 % 10, m[*i] / 1000 % 10);
                let (x, y) = (load(&m, m[*i + 1], p)?, load(&m, m[*i + 2], q)?);

                if x == 0 {
                    jump = Some(y);
                }

                3
            }
            /* setl */
            7 => {
                let (p, q) = (m[*i] / 100 % 10, m[*i] / 1000 % 10);
                let (x, y) = (load(&m, m[*i + 1], p)?, load(&m, m[*i + 2], q)?);
                let address = m[*i + 3];
                store(&mut m, address, (x < y).into())?;

                4
            }
            /* sete */
            8 => {
                let (p, q) = (m[*i] / 100 % 10, m[*i] / 1000 % 10);
                let (x, y) = (load(&m, m[*i + 1], p)?, load(&m, m[*i + 2], q)?);
                let address = m[*i + 3];
                store(&mut m, address, (x == y).into())?;

                4
            }
            /* hlt */
            99 => break,
            x => bail!("bad opcode {}", x),
        };

        if let Some(j) = jump {
            *i = j.try_into()?;
        }

        if out {
            return Ok(true);
        }
    }

    fn read(input: &mut Program<impl Read>) -> Result<isize, Error> {
        input.next().unwrap()
    }

    fn write(output: &mut impl Write, value: isize) -> Result<(), Error> {
        Ok(writeln!(output, "{}", value)?)
    }

    fn store(m: &mut [isize], address: isize, value: isize) -> Result<(), Error> {
        // eprintln!("store ${} {}", address, value);

        m[usize::try_from(address)?] = value;

        Ok(())
    }

    fn load(m: &[isize], parameter: isize, mode: isize) -> Result<isize, Error> {
        // match mode {
        //     0 => eprintln!("load ${} {}", parameter, m[usize::try_from(parameter)?]),
        //     1 => eprintln!("load {}", parameter),
        //     x => bail!("bad parameter mode {}", x),
        // }

        Ok(match mode {
            0 => m[usize::try_from(parameter)?],
            1 => parameter,
            x => bail!("bad parameter mode {}", x),
        })
    }

    Ok(false)
}

#[cfg(test)]
mod test {
    use failure::Error;

    #[test]
    fn run() -> Result<(), Error> {
        use super::run;

        let mut counter = 0;
        let mut output = vec![];
        let mut memory = vec![
            1101, 6, 7, 0, 4, 0, 102, 3, 0, 1, 101, 3, 1, 1, 4, 1, 102, 2, 0, 0, 101, 1, 1, 1, 1,
            1, 0, 0, 4, 0, 99, 4, 0,
        ];

        assert_eq!(
            run(&mut memory, &mut counter, &mut &b""[..], &mut output)?,
            true
        );
        assert_eq!(counter, 6);

        assert_eq!(
            run(&mut memory, &mut counter, &mut &b""[..], &mut output)?,
            true
        );
        assert_eq!(counter, 16);

        assert_eq!(
            run(&mut memory, &mut counter, &mut &b""[..], &mut output)?,
            true
        );
        assert_eq!(counter, 30);

        assert_eq!(
            run(&mut memory, &mut counter, &mut &b""[..], &mut output)?,
            false
        );
        assert_eq!(counter, 30);

        assert_eq!(output, b"13\n42\n69\n");

        Ok(())
    }

    #[test]
    fn jcc() -> Result<(), Error> {
        use super::run;

        let mut output = vec![];
        let memory = vec![
            3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0,
            0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4,
            20, 1105, 1, 46, 98, 99,
        ];

        assert_eq!(
            run(&mut memory.clone(), &mut 0, &mut &b"7"[..], &mut output)?,
            true
        );
        assert_eq!(output, b"999\n");

        output.clear();

        assert_eq!(
            run(&mut memory.clone(), &mut 0, &mut &b"8"[..], &mut output)?,
            true
        );
        assert_eq!(output, b"1000\n");

        output.clear();

        assert_eq!(
            run(&mut memory.clone(), &mut 0, &mut &b"9"[..], &mut output)?,
            true
        );
        assert_eq!(output, b"1001\n");

        Ok(())
    }
}
