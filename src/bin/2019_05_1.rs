use std::convert::TryFrom;
use std::env::args;
use std::fs::File;
use std::io::{stdin, BufRead, BufReader};

use failure::{bail, Error};

fn main() -> Result<(), Error> {
    let p = BufReader::new(File::open(args().nth(1).unwrap())?);
    let p: Vec<_> = Program::new(p).collect::<Result<_, _>>()?;

    let input = stdin();
    let input = Program::new(input.lock());

    run(&p, input)?;

    Ok(())
}

fn run(program: &[isize], mut input: Program<impl BufRead>) -> Result<(), Error> {
    let mut m = program.to_owned();

    let mut i = 0;

    while i < m.len() {
        i += match m[i] % 100 {
            1 => {
                let (p, q) = (m[i] / 100 % 10, m[i] / 1000 % 10);
                let (x, y) = (load(&m, m[i + 1], p)?, load(&m, m[i + 2], q)?);
                let address = m[i + 3];
                store(&mut m, address, x + y)?;

                4
            }
            2 => {
                let (p, q) = (m[i] / 100 % 10, m[i] / 1000 % 10);
                let (x, y) = (load(&m, m[i + 1], p)?, load(&m, m[i + 2], q)?);
                let address = m[i + 3];
                store(&mut m, address, x * y)?;

                4
            }
            3 => {
                let address = m[i + 1];
                store(&mut m, address, read(&mut input)?)?;

                2
            }
            4 => {
                let p = m[i] / 100 % 10;
                write(load(&m, m[i + 1], p)?);

                2
            }
            99 => break,
            x => bail!("bad opcode {}", x),
        };
    }

    fn read(input: &mut Program<impl BufRead>) -> Result<isize, Error> {
        input.next().unwrap()
    }

    fn write(value: isize) {
        println!("{}", value);
    }

    fn store(m: &mut [isize], address: isize, value: isize) -> Result<(), Error> {
        m[usize::try_from(address)?] = value;

        Ok(())
    }

    fn load(m: &[isize], parameter: isize, mode: isize) -> Result<isize, Error> {
        Ok(match mode {
            0 => m[usize::try_from(parameter)?],
            1 => parameter,
            x => bail!("bad parameter mode {}", x),
        })
    }

    Ok(())
}

struct Program<I> {
    inner: I,
}

impl<I> Program<I> {
    fn new(inner: I) -> Self {
        Self { inner }
    }
}

impl<I: BufRead> Iterator for Program<I> {
    type Item = Result<isize, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut result: Option<isize> = None;
        let mut factor = 1;

        loop {
            result = match digit(&mut self.inner) {
                Ok(Some(q)) if q == b'-'.into() => {
                    factor = -1;

                    result
                }
                Ok(Some(q)) => result.map_or_else(
                    || Some(q - isize::from(b'0')),
                    |p| Some(q - isize::from(b'0') + p * 10),
                ),
                Ok(None) => return result.map(|x| Ok(x * factor)),
                Err(x) => return Some(Err(x)),
            };
        }

        fn digit(source: &mut impl BufRead) -> Result<Option<isize>, Error> {
            let mut scratch = [0u8];

            match source.read(&mut scratch) {
                Ok(0) => Ok(None),
                Ok(1) => match scratch[0] {
                    b',' | b'\n' | b'\r' => Ok(None),
                    x @ b'-' | x @ b'0'..=b'9' => Ok(Some(x.into())),
                    x => bail!("unexpected {:?}", x),
                },
                Ok(_) => unreachable!(),
                Err(x) => bail!("I/O error {:?}", x),
            }
        }
    }
}
