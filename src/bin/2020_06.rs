use std::collections::HashMap;
use std::io::{stdin, BufRead};

use jane_eyre::Result;

fn main() -> Result<()> {
    let mut result = 0;
    let mut members = 0usize;
    let mut questions = HashMap::new();

    for line in stdin().lock().lines() {
        let line = line?;

        if line.is_empty() {
            result += drain_count(&mut questions, members);
            members = 0;
            questions.clear();
            continue;
        }

        members += 1;

        for question in line.chars() {
            questions
                .entry(question)
                .and_modify(|x| *x += 1)
                .or_insert(1usize);
        }
    }

    println!("{}", result + drain_count(&mut questions, members));

    Ok(())
}

fn drain_count(questions: &mut HashMap<char, usize>, members: usize) -> usize {
    questions
        .drain()
        .filter(|&(_, count)| !advent::part2() || count == members)
        .count()
}
