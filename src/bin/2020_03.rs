use std::io::{stdin, BufRead};

use jane_eyre::Result;

fn main() -> Result<()> {
    let map = stdin()
        .lock()
        .lines()
        .collect::<Result<Vec<_>, std::io::Error>>()?;

    let mut result = 1;

    for &(right, down) in &[(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)] {
        result *= dbg!(map
            .iter()
            .step_by(down)
            .map(|x| x.as_bytes())
            .zip((0..).step_by(right))
            .filter(|(x, i)| x[i % x.len()] == b'#')
            .count());
    }

    println!("{}", result);

    Ok(())
}
