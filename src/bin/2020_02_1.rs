use std::io::{stdin, BufRead};

use itertools::Itertools;

use advent::fatal::FatalWrapErr;
use advent::regex::CapturesOwned;

fn main() -> jane_eyre::Result<()> {
    let pattern = regex::Regex::new("([0-9]+)-([0-9]+) ([^:]+): (.+)")?;

    #[rustfmt::skip]
    let result = stdin()
        .lock()
        .lines()
        .map(|x| x.fatal("failed to read input"))
        .flat_map(|x| pattern.captures_owned(&*x))
        .tuples()
        .map(|(min, max, needle, haystack)| (
            min.parse::<usize>().fatal("first field failed to parse"),
            max.parse::<usize>().fatal("second field failed to parse"),
            haystack.match_indices(&needle).count(),
        ))
        .filter(|(min, max, actual)| min <= actual && actual <= max)
        .count();

    println!("{}", result);

    Ok(())
}
