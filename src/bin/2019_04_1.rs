use std::io::{stdin, BufRead};

use failure::Error;

fn main() -> Result<(), Error> {
    let input = stdin().lock().lines().next().unwrap()?;
    let mut input = input.split('-');

    let p: usize = input.next().unwrap().parse()?;
    let q: usize = input.next().unwrap().parse()?;

    println!("{}", (p..=q).filter(|&x| predicate(x)).count());

    Ok(())
}

fn predicate(password: usize) -> bool {
    let password = password.to_string();

    if password.len() != 6 {
        return false;
    }

    let mut monotonic = true;
    let mut double = false;

    password.bytes().fold(0, |p, q| {
        if p > q {
            monotonic = false;
        }

        if p == q {
            double = true;
        }

        q
    });

    monotonic && double
}
