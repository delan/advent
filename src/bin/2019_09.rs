use std::convert::{TryFrom, TryInto};
use std::env::args;
use std::fs::File;
use std::io::{stdin, stdout, BufReader, Read, Write};

use failure::{bail, Error};

use advent::intcode::Program;

fn main() -> Result<(), Error> {
    let p = BufReader::new(File::open(args().nth(1).unwrap())?);
    let mut p: Vec<_> = Program::new(p).collect::<Result<_, _>>()?;

    let mut counter = 0;
    let mut relative = 0;

    while run(
        &mut p,
        &mut counter,
        &mut relative,
        &mut stdin(),
        &mut stdout(),
    )? {}

    Ok(())
}

fn run(
    memory: &mut Vec<isize>,
    counter: &mut usize,
    relative: &mut isize,
    input: &mut impl Read,
    mut output: &mut impl Write,
) -> Result<bool, Error> {
    let mut m = memory;
    let i = counter;
    let mut input = Program::new(input);

    loop {
        if *i >= m.len() {
            bail!("program counter {} out of bounds", *i);
        }

        let mut jump = None;
        let mut out = false;

        *i += match m[*i] % 100 {
            /* add */
            1 => {
                let (p, q, r) = (m[*i] / 100 % 10, m[*i] / 1000 % 10, m[*i] / 10000 % 10);
                let (x, y) = (
                    load(&m, m[*i + 1], p, *relative)?,
                    load(&m, m[*i + 2], q, *relative)?,
                );
                let parameter = m[*i + 3];
                store(&mut m, parameter, r, *relative, x + y)?;

                4
            }
            /* mul */
            2 => {
                let (p, q, r) = (m[*i] / 100 % 10, m[*i] / 1000 % 10, m[*i] / 10000 % 10);
                let (x, y) = (
                    load(&m, m[*i + 1], p, *relative)?,
                    load(&m, m[*i + 2], q, *relative)?,
                );
                let parameter = m[*i + 3];
                store(&mut m, parameter, r, *relative, x * y)?;

                4
            }
            /* in */
            3 => {
                let p = m[*i] / 100 % 10;
                let parameter = m[*i + 1];
                store(&mut m, parameter, p, *relative, read(&mut input)?)?;

                2
            }
            /* out */
            4 => {
                let p = m[*i] / 100 % 10;
                write(&mut output, load(&m, m[*i + 1], p, *relative)?)?;
                out = true;

                2
            }
            /* jnz */
            5 => {
                let (p, q) = (m[*i] / 100 % 10, m[*i] / 1000 % 10);
                let (x, y) = (
                    load(&m, m[*i + 1], p, *relative)?,
                    load(&m, m[*i + 2], q, *relative)?,
                );

                if x != 0 {
                    jump = Some(y);
                }

                3
            }
            /* jz */
            6 => {
                let (p, q) = (m[*i] / 100 % 10, m[*i] / 1000 % 10);
                let (x, y) = (
                    load(&m, m[*i + 1], p, *relative)?,
                    load(&m, m[*i + 2], q, *relative)?,
                );

                if x == 0 {
                    jump = Some(y);
                }

                3
            }
            /* setl */
            7 => {
                let (p, q, r) = (m[*i] / 100 % 10, m[*i] / 1000 % 10, m[*i] / 10000 % 10);
                let (x, y) = (
                    load(&m, m[*i + 1], p, *relative)?,
                    load(&m, m[*i + 2], q, *relative)?,
                );
                let parameter = m[*i + 3];
                store(&mut m, parameter, r, *relative, (x < y).into())?;

                4
            }
            /* sete */
            8 => {
                let (p, q, r) = (m[*i] / 100 % 10, m[*i] / 1000 % 10, m[*i] / 10000 % 10);
                let (x, y) = (
                    load(&m, m[*i + 1], p, *relative)?,
                    load(&m, m[*i + 2], q, *relative)?,
                );
                let parameter = m[*i + 3];
                store(&mut m, parameter, r, *relative, (x == y).into())?;

                4
            }
            9 => {
                let p = m[*i] / 100 % 10;
                *relative += load(&m, m[*i + 1], p, *relative)?;

                2
            }
            /* hlt */
            99 => break,
            x => bail!("bad opcode {}", x),
        };

        if let Some(j) = jump {
            *i = j.try_into()?;
        }

        if out {
            return Ok(true);
        }
    }

    fn read(input: &mut Program<impl Read>) -> Result<isize, Error> {
        input.next().unwrap()
    }

    fn write(output: &mut impl Write, value: isize) -> Result<(), Error> {
        Ok(writeln!(output, "{}", value)?)
    }

    fn store(
        m: &mut Vec<isize>,
        parameter: isize,
        mode: isize,
        relative: isize,
        value: isize,
    ) -> Result<(), Error> {
        // eprintln!("store ${} {}", address, value);

        let offset = match mode {
            0 => usize::try_from(parameter)?,
            2 => usize::try_from(relative + parameter)?,
            x => bail!("bad parameter mode {}", x),
        };

        if offset >= m.len() {
            m.resize(1 + offset, 0);
        }

        m[offset] = value;

        Ok(())
    }

    fn load(
        m: &Vec<isize>,
        parameter: isize,
        mode: isize,
        relative: isize,
    ) -> Result<isize, Error> {
        // match mode {
        //     0 => eprintln!("load ${} {}", parameter, m[usize::try_from(parameter)?]),
        //     1 => eprintln!("load {}", parameter),
        //     x => bail!("bad parameter mode {}", x),
        // }

        if mode == 1 {
            return Ok(parameter);
        }

        let offset = match mode {
            0 => usize::try_from(parameter)?,
            2 => usize::try_from(relative + parameter)?,
            x => bail!("bad parameter mode {}", x),
        };

        if offset >= m.len() {
            return Ok(0);
        }

        Ok(m[offset])
    }

    Ok(false)
}
