use std::collections::HashSet;
use std::io::{stdin, BufRead};

use jane_eyre::Result;

fn main() -> Result<()> {
    let mut seen = HashSet::<isize>::default();

    for line in stdin().lock().lines() {
        seen.insert(line?.parse()?);
    }

    for i in &seen {
        for j in &seen {
            if let Some(k) = seen.get(&(2020 - i - j)) {
                println!("{}", i * j * k);
                return Ok(());
            }
        }
    }

    panic!();
}
