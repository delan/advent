use std::cmp::max;
use std::convert::{TryFrom, TryInto};
use std::env::args;
use std::fs::File;
use std::io::{BufReader, Read, Write};
use std::iter::repeat;
use std::str;

use failure::{bail, Error};

fn main() -> Result<(), Error> {
    let p = BufReader::new(File::open(args().nth(1).unwrap())?);
    let p: Vec<_> = Program::new(p).collect::<Result<_, _>>()?;

    let mut x = (0..5).collect::<Vec<_>>();
    let mut result = thrusters(&p, &x)?;

    while plain_changes(&mut x) {
        result = max(result, thrusters(&p, &x)?);
    }

    println!("{}", result);

    Ok(())
}

fn plain_changes(permutation: &mut [isize]) -> bool {
    let n = permutation.len();
    let mut x = repeat(0).take(n).collect::<Vec<_>>();
    let mut y = repeat(0).take(n).collect::<Vec<_>>();

    for (p, &i) in permutation.iter().enumerate() {
        let stop = i - 1;
        let i = usize::try_from(i).unwrap();
        x[i] = isize::try_from(p).unwrap();
        y[i] = x[i] - signum(permutation, stop);
    }

    for i in (0..n).rev() {
        let n = n.try_into().unwrap();

        if 0 <= y[i] && y[i] < n {
            let y = y[i].try_into().unwrap();

            if permutation[y] < i.try_into().unwrap() {
                let x = x[i].try_into().unwrap();
                permutation.swap(x, y);
                return true;
            }
        }
    }

    false
}

fn signum(permutation: &[isize], maximum: isize) -> isize {
    let mut result = 0;

    for i in 0..permutation.len() {
        for j in (i + 1)..permutation.len() {
            if maximum >= permutation[i] && permutation[i] > permutation[j] {
                result += 1;
            }
        }
    }

    1 - result % 2 * 2
}

fn thrusters(program: &[isize], settings: &[isize]) -> Result<isize, Error> {
    let mut result = 0;

    for &setting in settings {
        result = amplify(program, setting, result)?;
    }

    Ok(result)
}

fn amplify(program: &[isize], setting: isize, signal: isize) -> Result<isize, Error> {
    let input = format!("{},{}", setting, signal);
    let mut output = vec![];
    run(program, &mut input.as_bytes(), &mut output)?;

    Ok(isize::from_str_radix(str::from_utf8(&output)?.trim(), 10)?)
}

fn run(program: &[isize], input: &mut impl Read, mut output: &mut impl Write) -> Result<(), Error> {
    let mut m = program.to_owned();
    let mut input = Program::new(input);

    let mut i = 0;

    while i < m.len() {
        let mut jump = None;

        i += match m[i] % 100 {
            /* add */
            1 => {
                let (p, q) = (m[i] / 100 % 10, m[i] / 1000 % 10);
                let (x, y) = (load(&m, m[i + 1], p)?, load(&m, m[i + 2], q)?);
                let address = m[i + 3];
                store(&mut m, address, x + y)?;

                4
            }
            /* imul */
            2 => {
                let (p, q) = (m[i] / 100 % 10, m[i] / 1000 % 10);
                let (x, y) = (load(&m, m[i + 1], p)?, load(&m, m[i + 2], q)?);
                let address = m[i + 3];
                store(&mut m, address, x * y)?;

                4
            }
            /* in */
            3 => {
                let address = m[i + 1];
                store(&mut m, address, read(&mut input)?)?;

                2
            }
            /* out */
            4 => {
                let p = m[i] / 100 % 10;
                write(&mut output, load(&m, m[i + 1], p)?)?;

                2
            }
            /* jnz */
            5 => {
                let (p, q) = (m[i] / 100 % 10, m[i] / 1000 % 10);
                let (x, y) = (load(&m, m[i + 1], p)?, load(&m, m[i + 2], q)?);

                if x != 0 {
                    jump = Some(y);
                }

                3
            }
            /* jz */
            6 => {
                let (p, q) = (m[i] / 100 % 10, m[i] / 1000 % 10);
                let (x, y) = (load(&m, m[i + 1], p)?, load(&m, m[i + 2], q)?);

                if x == 0 {
                    jump = Some(y);
                }

                3
            }
            /* setl */
            7 => {
                let (p, q) = (m[i] / 100 % 10, m[i] / 1000 % 10);
                let (x, y) = (load(&m, m[i + 1], p)?, load(&m, m[i + 2], q)?);
                let address = m[i + 3];
                store(&mut m, address, (x < y).into())?;

                4
            }
            /* sete */
            8 => {
                let (p, q) = (m[i] / 100 % 10, m[i] / 1000 % 10);
                let (x, y) = (load(&m, m[i + 1], p)?, load(&m, m[i + 2], q)?);
                let address = m[i + 3];
                store(&mut m, address, (x == y).into())?;

                4
            }
            99 => break,
            x => bail!("bad opcode {}", x),
        };

        if let Some(j) = jump {
            i = j.try_into()?;
        }
    }

    fn read(input: &mut Program<impl Read>) -> Result<isize, Error> {
        input.next().unwrap()
    }

    fn write(output: &mut impl Write, value: isize) -> Result<(), Error> {
        Ok(writeln!(output, "{}", value)?)
    }

    fn store(m: &mut [isize], address: isize, value: isize) -> Result<(), Error> {
        m[usize::try_from(address)?] = value;

        Ok(())
    }

    fn load(m: &[isize], parameter: isize, mode: isize) -> Result<isize, Error> {
        Ok(match mode {
            0 => m[usize::try_from(parameter)?],
            1 => parameter,
            x => bail!("bad parameter mode {}", x),
        })
    }

    Ok(())
}

struct Program<I> {
    inner: I,
}

impl<I> Program<I> {
    fn new(inner: I) -> Self {
        Self { inner }
    }
}

impl<I: Read> Iterator for Program<I> {
    type Item = Result<isize, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut result: Option<isize> = None;
        let mut factor = 1;

        loop {
            result = match digit(&mut self.inner) {
                Ok(Some(q)) if q == b'-'.into() => {
                    factor = -1;

                    result
                }
                Ok(Some(q)) => result.map_or_else(
                    || Some(q - isize::from(b'0')),
                    |p| Some(q - isize::from(b'0') + p * 10),
                ),
                Ok(None) => return result.map(|x| Ok(x * factor)),
                Err(x) => return Some(Err(x)),
            };
        }

        fn digit(source: &mut impl Read) -> Result<Option<isize>, Error> {
            let mut scratch = [0u8];

            match source.read(&mut scratch) {
                Ok(0) => Ok(None),
                Ok(1) => match scratch[0] {
                    b',' | b'\n' | b'\r' => Ok(None),
                    x @ b'-' | x @ b'0'..=b'9' => Ok(Some(x.into())),
                    x => bail!("unexpected {:?}", x),
                },
                Ok(_) => unreachable!(),
                Err(x) => bail!("I/O error {:?}", x),
            }
        }
    }
}
