use std::io::{stdin, BufRead};

use itertools::Itertools;

use advent::fatal::FatalWrapErr;
use advent::regex::CapturesOwned;

fn main() -> jane_eyre::Result<()> {
    let pattern = regex::Regex::new("([0-9]+)-([0-9]+) ([^:]+): (.+)")?;

    #[rustfmt::skip]
    let result = stdin()
        .lock()
        .lines()
        .map(|x| x.fatal("failed to read input"))
        .flat_map(|x| pattern.captures_owned(&*x))
        .tuples()
        .map(|(i, j, needle, haystack)| (
            i.parse::<usize>().fatal("first field failed to parse"),
            j.parse::<usize>().fatal("second field failed to parse"),
            needle,
            haystack,
        ))
        .map(|(i, j, needle, haystack)| (
            haystack[i - 1..].starts_with(&needle),
            haystack[j - 1..].starts_with(&needle),
        ))
        .filter(|(i, j)| i != j)
        .count();

    println!("{}", result);

    Ok(())
}
