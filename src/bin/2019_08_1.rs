use std::io::{stdin, Read};
use std::str;

use failure::Error;

fn main() -> Result<(), Error> {
    let mut source = vec![];

    stdin().read_to_end(&mut source)?;

    let source = str::from_utf8(&source)?.trim();
    let mut best = None;
    let mut count = usize::max_value();

    for layer in source.as_bytes().chunks(25 * 6) {
        if occurrences(layer, b'0') < count {
            best = Some(layer);
            count = occurrences(layer, b'0');
        }
    }

    let best = best.unwrap();

    println!("{}", occurrences(best, b'1') * occurrences(best, b'2'));

    Ok(())
}

fn occurrences(source: &[u8], digit: u8) -> usize {
    source.iter().copied().filter(|&x| x == digit).count()
}
