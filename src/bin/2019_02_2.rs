use std::io::{stdin, BufRead};

use failure::{bail, Error};

fn main() -> Result<(), Error> {
    let p: Vec<_> = Program::new(stdin().lock()).collect::<Result<_, _>>()?;

    for noun in 0..100 {
        for verb in 0..100 {
            if run(&p, noun, verb)? == 19690720 {
                println!("{}", 100 * noun + verb);
            }
        }
    }

    Ok(())
}

fn run(program: &[usize], noun: usize, verb: usize) -> Result<usize, Error> {
    let mut m = program.to_owned();

    m[1] = noun;
    m[2] = verb;

    let mut i = 0;

    while i < m.len() {
        match m[i] {
            1 => {
                let (r, x, y) = (m[i + 3], m[i + 1], m[i + 2]);
                m[r] = m[x] + m[y];
            }
            2 => {
                let (r, x, y) = (m[i + 3], m[i + 1], m[i + 2]);
                m[r] = m[x] * m[y];
            }
            99 => break,
            x => bail!("bad opcode {}", x),
        }

        i += 4;
    }

    Ok(m[0])
}

struct Program<I> {
    inner: I,
}

impl<I> Program<I> {
    fn new(inner: I) -> Self {
        Self { inner }
    }
}

impl<I: BufRead> Iterator for Program<I> {
    type Item = Result<usize, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut result: Option<usize> = None;

        loop {
            result = match digit(&mut self.inner) {
                Ok(Some(q)) => result.map_or_else(|| Some(q), |p| Some(10 * p + q)),
                Ok(None) => return result.map(Ok),
                Err(x) => return Some(Err(x)),
            };
        }

        fn digit(source: &mut impl BufRead) -> Result<Option<usize>, Error> {
            let mut scratch = [0u8];

            match source.read(&mut scratch) {
                Ok(0) => Ok(None),
                Ok(1) => match scratch[0] {
                    b',' | b'\n' | b'\r' => Ok(None),
                    x @ b'0'..=b'9' => Ok(Some((x - b'0').into())),
                    x => bail!("unexpected {:?}", x),
                },
                Ok(_) => unreachable!(),
                Err(x) => bail!("I/O error {:?}", x),
            }
        }
    }
}
