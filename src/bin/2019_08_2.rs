use std::io::{stdin, Read};
use std::str;

use failure::Error;

fn main() -> Result<(), Error> {
    let mut source = vec![];

    stdin().read_to_end(&mut source)?;

    let (width, height) = (25, 6);
    let source = str::from_utf8(&source)?.trim();
    let mut sink = vec![b'2'; width * height];

    for source in source.as_bytes().chunks(width * height).rev() {
        for i in 0..source.len() {
            sink[i] = match source[i] {
                b'2' => sink[i],
                x => x,
            };
        }
    }

    println!("! XPM2");
    println!("{} {} 3 1", width, height);
    println!("0 c #000000");
    println!("1 c #FFFFFF");
    println!("2 c #FF00FF");

    for row in sink.chunks(width) {
        println!("{}", str::from_utf8(row)?);
    }

    Ok(())
}
