use std::collections::HashMap;
use std::io::{stdin, BufRead};

use failure::Error;

fn main() -> Result<(), Error> {
    let mut parents = HashMap::new();

    for line in stdin().lock().lines() {
        let line = line?;
        let mut line = line.split(')');
        let parent = line.next().unwrap().to_owned();
        let child = line.next().unwrap().to_owned();
        parents.insert(child, parent);
    }

    let you = path(&parents, "YOU");
    let san = path(&parents, "SAN");

    let common = you
        .iter()
        .rev()
        .zip(san.iter().rev())
        .filter(|(p, q)| p == q)
        .fuse()
        .count();

    let you = you.len() - common;
    let san = san.len() - common;

    println!("{}", you + san);

    Ok(())
}

fn path<'p: 'o, 'o>(parents: &'p HashMap<String, String>, mut object: &'o str) -> Vec<&'o str> {
    let mut result = Vec::default();

    while object != "COM" {
        object = &parents[object];
        result.push(object);
    }

    result
}
