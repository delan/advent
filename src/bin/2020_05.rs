use std::collections::BTreeSet;
use std::io::{stdin, BufRead};
use std::ops::Range;

use jane_eyre::Result;

use advent::fatal::FatalOption;

fn main() -> Result<()> {
    let mut highest = 0;
    let mut seen = BTreeSet::default();

    for line in stdin().lock().lines() {
        let line = line?;
        let mut line = line.chars();
        let mut row = 0..128usize; // F 0 ..= 127 B
        let mut column = 0..8usize; // L 0..= 7 R

        for _ in 0..7 {
            let direction = line.next().fatal("failed to read row direction");
            row = traverse(row, 'F', 'B', direction).fatal("bad row direction");
        }

        for _ in 0..3 {
            let direction = line.next().fatal("failed to read column direction");
            column = traverse(column, 'L', 'R', direction).fatal("bad column direction");
        }

        let id = id(&row, &column);
        highest = std::cmp::max(highest, id);
        seen.insert(id);
    }

    if !advent::part2() {
        println!("{}", highest);
    } else {
        // find a potential left seat
        for &id in &seen {
            // potential left/my/right seats must be in the same row
            if row(id) != row(id + 1) || row(id + 1) != row(id + 2) {
                continue;
            }

            // my seat must be missing and right seat must be present
            if seen.contains(&(id + 1)) || !seen.contains(&(id + 2)) {
                continue;
            }

            // found my seat
            println!("{}", id + 1);
            return Ok(());
        }

        panic!();
    }

    Ok(())
}

fn traverse(range: Range<usize>, low: char, high: char, direction: char) -> Option<Range<usize>> {
    match direction {
        x if x == low => Some(range.start..half(&range)),
        x if x == high => Some(half(&range)..range.end),
        _ => None,
    }
}

fn half(Range { start, end }: &Range<usize>) -> usize {
    // overflow-safe
    start + (end - start) / 2
}

fn id(row: &Range<usize>, column: &Range<usize>) -> usize {
    debug_assert!(row.len() == 1, "exactly one row should be given");
    debug_assert!(column.len() == 1, "exactly one column should be given");

    row.start * 8 + column.start
}

fn row(id: usize) -> usize {
    id / 8
}
