use std::error::Error;
use std::io::{stdin, BufRead};

fn main() -> Result<(), Box<dyn Error>> {
    let mut result = 0;

    for line in stdin().lock().lines() {
        result += line?.parse::<usize>()? / 3 - 2;
    }

    println!("{}", result);

    Ok(())
}
