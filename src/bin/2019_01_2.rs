use std::error::Error;
use std::io::{stdin, BufRead};

fn main() -> Result<(), Box<dyn Error>> {
    // let result = stdin().lock().lines()
    //                            .map(|x| x.map_err(Box::<dyn Error>::from).and_then(|x| x.parse().map_err(Box::<dyn Error>::from)).map(fuel))
    //                            .fold(Ok(0), |a, x| a.and_then(|a| x.map(|x| x + a)));
    // println!("{}", result?);

    // let lines: Result<Vec<_>, _> = stdin().lock().lines().collect();
    // let result: Result<Vec<_>, _> = lines?.iter().map(|x| x.parse::<isize>()).collect();
    // println!("{}", result?.iter().copied().map(fuel).sum::<isize>());

    // println!("{}", stdin().lock().lines().try_fold(0, |a, x| Ok::<_, Box<dyn Error>>(a + fuel(x?.parse::<isize>()?)))?);

    let mut result = 0;

    for line in stdin().lock().lines() {
        let mut delta = fuel(line?.parse()?);

        while delta > 0 {
            result += delta;
            delta = fuel(delta);
        }
    }

    println!("{}", result);

    Ok(())
}

fn fuel(mass: isize) -> isize {
    mass / 3 - 2
}
