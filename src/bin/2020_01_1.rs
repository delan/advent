use std::collections::HashSet;
use std::io::{stdin, BufRead};

use jane_eyre::Result;

fn main() -> Result<()> {
    let mut seen = HashSet::<isize>::default();

    for line in stdin().lock().lines() {
        let x = line?.parse()?;

        if let Some(y) = seen.get(&(2020 - x)) {
            println!("{}", x * y);
            return Ok(());
        }

        seen.insert(x);
    }

    panic!();
}
