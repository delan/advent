use std::collections::HashSet;
use std::io::{stdin, BufRead};

use jane_eyre::Result;

#[allow(non_snake_case)]
fn REQUIRED() -> HashSet<&'static str> {
    set(["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"])
}

#[allow(non_snake_case)]
fn OPTIONAL() -> HashSet<&'static str> {
    set(["cid"])
}

fn main() -> Result<()> {
    let mut result = 0;
    let mut required = REQUIRED();
    let mut optional = OPTIONAL();

    for line in stdin().lock().lines() {
        let line = line?;

        if line.is_empty() {
            if required.is_empty() {
                result += 1;
            }

            required = REQUIRED();
            optional = OPTIONAL();
        }

        line.split_ascii_whitespace()
            .filter_map(filter_validate)
            // true allows unknown field keys
            .all(|x| required.remove(&*x) || optional.remove(&*x) || true);
    }

    if required.is_empty() {
        result += 1;
    }

    println!("{}", result);

    Ok(())
}

fn filter_validate(pair: &str) -> Option<&str> {
    let colon = pair.find(':');
    let end = pair.len();
    let key = &pair[..colon.unwrap_or(end)];
    let value = &pair[colon.map_or(end, |x| x + 1)..];

    let valid = match (advent::part2(), key, value) {
        (false, _, _) => true,
        (_, "byr", x) => validate_urange(1920, x, 2002),
        (_, "iyr", x) => validate_urange(2010, x, 2020),
        (_, "eyr", x) => validate_urange(2020, x, 2030),
        (_, "hgt", x) if x.ends_with("cm") => validate_urange(150, &x[..x.len() - "cm".len()], 193),
        (_, "hgt", x) if x.ends_with("in") => validate_urange(59, &x[..x.len() - "in".len()], 76),
        (_, "hgt", _) => false, // i was missing this fucking arm
        (_, "hcl", x) => validate_generic(x, "#", 6, |x| x.is_ascii_hexdigit()),
        (_, "ecl", x) => ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&x),
        (_, "pid", x) => validate_generic(x, "", 9, |x| x.is_ascii_digit()),
        (_, "cid", _) => true,
        (true, _, _) => true, // unknown key
    };

    if !valid {
        eprintln!("invalid {}: {}", key, value);
    }

    Some(key).filter(|_| valid)
}

fn set<'t, T: std::hash::Hash + Eq + ?Sized>(xs: impl AsRef<[&'t T]>) -> HashSet<&'t T> {
    xs.as_ref().iter().cloned().collect()
}

fn validate_urange(min: usize, x: &str, max: usize) -> bool {
    x.parse::<usize>().map_or(false, |x| min <= x && x <= max)
}

fn validate_generic(
    x: &str,
    prefix: impl AsRef<str>,
    len: usize,
    predicate: impl Fn(char) -> bool,
) -> bool {
    x.strip_prefix(prefix.as_ref())
        .map_or(false, |x| x.len() == len && x.chars().all(predicate))
}
