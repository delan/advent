use std::cmp::Ordering;
use std::collections::BTreeSet;
use std::convert::TryFrom;
use std::io::{stdin, BufRead};
use std::str::FromStr;

use failure::{bail, Error};

fn main() -> Result<(), Error> {
    let stdin = stdin();
    let mut lines = stdin.lock().lines();

    let p = lines.next().unwrap()?;
    let q = lines.next().unwrap()?;

    let p: Vec<Step> = p.split(',').map(|x| x.parse()).collect::<Result<_, _>>()?;
    let q: Vec<Step> = q.split(',').map(|x| x.parse()).collect::<Result<_, _>>()?;

    let (p, _) = p.iter().fold((BTreeSet::default(), Point(0, 0)), walk);
    let (q, _) = q.iter().fold((BTreeSet::default(), Point(0, 0)), walk);

    println!("{:?}", p.intersection(&q).next().unwrap().distance());

    Ok(())
}

fn walk((mut points, mut end): (BTreeSet<Point>, Point), step: &Step) -> (BTreeSet<Point>, Point) {
    for _ in 0..step.magnitude {
        match step.direction {
            Direction::Up => end.1 += 1,
            Direction::Down => end.1 -= 1,
            Direction::Left => end.0 -= 1,
            Direction::Right => end.0 += 1,
        }

        points.insert(end);
    }

    (points, end)
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
struct Point(isize, isize);

#[derive(Debug)]
struct Step {
    direction: Direction,
    magnitude: usize,
}

#[derive(Debug, enumn::N)]
#[repr(u8)]
enum Direction {
    Up = b'U',
    Down = b'D',
    Left = b'L',
    Right = b'R',
}

impl Point {
    fn distance(&self) -> usize {
        usize::try_from(self.0.abs())
            .unwrap()
            .checked_add(usize::try_from(self.1.abs()).unwrap())
            .unwrap()
    }
}

impl PartialOrd for Point {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Point {
    fn cmp(&self, other: &Self) -> Ordering {
        self.distance()
            .cmp(&other.distance())
            .then_with(|| self.0.cmp(&other.0))
            .then_with(|| self.1.cmp(&other.1))
    }
}

impl FromStr for Step {
    type Err = Error;

    fn from_str(source: &str) -> Result<Self, Self::Err> {
        let (direction, magnitude) = source.split_at(1);

        let direction = match direction.as_bytes() {
            [] => bail!("missing direction"),
            [x] => *x,
            _ => unreachable!(),
        };

        let direction = match Direction::n(direction) {
            Some(x) => x,
            None => bail!("bad direction {:?}", direction),
        };

        let magnitude = magnitude.parse()?;

        Ok(Step {
            direction,
            magnitude,
        })
    }
}
