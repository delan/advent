use std::collections::HashMap;
use std::io::{stdin, BufRead};

use failure::Error;

fn main() -> Result<(), Error> {
    let mut parents = HashMap::new();

    for line in stdin().lock().lines() {
        let line = line?;
        let mut line = line.split(')');
        let parent = line.next().unwrap().to_owned();
        let child = line.next().unwrap().to_owned();
        parents.insert(child, parent);
    }

    let mut result = 0;

    for (mut child, _) in parents.iter() {
        while child != "COM" {
            child = &parents[child];
            result += 1;
        }
    }

    println!("{}", result);

    Ok(())
}
