use std::cmp::min;
use std::collections::{BTreeSet, HashMap};
use std::io::{stdin, BufRead};
use std::str::FromStr;

use failure::{bail, Error};

fn main() -> Result<(), Error> {
    let stdin = stdin();
    let mut lines = stdin.lock().lines();

    let p = lines.next().unwrap()?;
    let q = lines.next().unwrap()?;

    let p: Vec<Step> = p.split(',').map(|x| x.parse()).collect::<Result<_, _>>()?;
    let q: Vec<Step> = q.split(',').map(|x| x.parse()).collect::<Result<_, _>>()?;

    let (p, m, _, _) = p.iter().fold(
        (BTreeSet::default(), HashMap::default(), 0, Point(0, 0)),
        walk,
    );
    let (q, n, _, _) = q.iter().fold(
        (BTreeSet::default(), HashMap::default(), 0, Point(0, 0)),
        walk,
    );

    let mut intersections: Vec<_> = p.intersection(&q).map(|x| m[x] + n[x]).collect();
    intersections.sort();

    println!("{:?}", intersections[0]);

    Ok(())
}

fn walk(
    (mut points, mut best, mut steps, mut end): (
        BTreeSet<Point>,
        HashMap<Point, usize>,
        usize,
        Point,
    ),
    step: &Step,
) -> (BTreeSet<Point>, HashMap<Point, usize>, usize, Point) {
    for _ in 0..step.magnitude {
        match step.direction {
            Direction::Up => end.1 += 1,
            Direction::Down => end.1 -= 1,
            Direction::Left => end.0 -= 1,
            Direction::Right => end.0 += 1,
        }

        points.insert(end);
        steps += 1;

        best.entry(end)
            .and_modify(|x| *x = min(*x, steps))
            .or_insert(steps);
    }

    (points, best, steps, end)
}

#[derive(Debug, Hash, PartialEq, PartialOrd, Eq, Ord, Clone, Copy)]
struct Point(isize, isize);

#[derive(Debug)]
struct Step {
    direction: Direction,
    magnitude: usize,
}

#[derive(Debug, enumn::N)]
#[repr(u8)]
enum Direction {
    Up = b'U',
    Down = b'D',
    Left = b'L',
    Right = b'R',
}

impl FromStr for Step {
    type Err = Error;

    fn from_str(source: &str) -> Result<Self, Self::Err> {
        let (direction, magnitude) = source.split_at(1);

        let direction = match direction.as_bytes() {
            [] => bail!("missing direction"),
            [x] => *x,
            _ => unreachable!(),
        };

        let direction = match Direction::n(direction) {
            Some(x) => x,
            None => bail!("bad direction {:?}", direction),
        };

        let magnitude = magnitude.parse()?;

        Ok(Step {
            direction,
            magnitude,
        })
    }
}
