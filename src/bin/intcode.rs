use std::io::{stdin, stdout};

use clap::{App, SubCommand};
use failure::Error;

use advent::intcode::{disassemble, Program};

fn main() -> Result<(), Error> {
    let arguments = App::new("intcode")
        .subcommand(SubCommand::with_name("dump"))
        .get_matches();

    if let Some(_) = arguments.subcommand_matches("dump") {
        let p: Vec<_> = Program::new(stdin()).collect::<Result<_, _>>()?;
        return disassemble(&mut stdout(), &p);
    }

    Ok(())
}
