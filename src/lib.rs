pub mod fatal;
pub mod intcode;
pub mod regex;

pub fn part2() -> bool {
    std::env::var_os("ADVENT_PART2").is_some()
}
